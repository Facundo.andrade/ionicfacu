import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Formulario3Page } from './formulario3.page';

const routes: Routes = [
  {
    path: '',
    component: Formulario3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Formulario3PageRoutingModule {}
