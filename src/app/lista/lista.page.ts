import { Component, OnInit } from '@angular/core';

interface Componente {
  icon: string;
  name: string;
  redirectTo: string;
}

@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {


  componentes: Componente[] = [
    {

      icon: 'american-football',
      name: 'Action Sheet',
      redirectTo: '/action-sheet'

    },
    {

      icon: 'calculator',
      name: 'Conversor',
      redirectTo: '/action-sheet'

    },
    {
      icon: 'list',
      name: 'Lista',
      redirectTo: '/listaapp'

    },
    {

      icon: 'arrow-undo',
      name: 'Return',
      redirectTo: '/home'

    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
