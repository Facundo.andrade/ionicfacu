import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Formulario4PageRoutingModule } from './formulario4-routing.module';

import { Formulario4Page } from './formulario4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Formulario4PageRoutingModule
  ],
  declarations: [Formulario4Page]
})
export class Formulario4PageModule {}
