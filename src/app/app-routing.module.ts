import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'detalle',
    loadChildren: () => import('./detalle/detalle.module').then( m => m.DetallePageModule)
  },
  {
    path: 'lista',
    loadChildren: () => import('./lista/lista.module').then( m => m.ListaPageModule)
  },
  {
    path: 'listaapp',
    loadChildren: () => import('./listaapp/listaapp.module').then( m => m.ListaappPageModule)
  },
  {
    path: 'formulario',
    loadChildren: () => import('./formulario/formulario.module').then( m => m.FormularioPageModule)
  },
  {
    path: 'formulario2',
    loadChildren: () => import('./formulario2/formulario2.module').then( m => m.Formulario2PageModule)
  },  {
    path: 'formulario3',
    loadChildren: () => import('./formulario3/formulario3.module').then( m => m.Formulario3PageModule)
  },
  {
    path: 'formulario4',
    loadChildren: () => import('./formulario4/formulario4.module').then( m => m.Formulario4PageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
