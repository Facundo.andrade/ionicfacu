import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { info } from '../formulario/formulario.page';

@Component({
  selector: 'app-formulario2',
  templateUrl: './formulario2.page.html',
  styleUrls: ['./formulario2.page.scss'],
})
export class Formulario2Page{

  dades2: info;
  data2:any;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data2 = this.router.getCurrentNavigation().extras.state.parametros; 
      }
    });
  }

  pasoDatos2() {

    console.log(this.data2)

    let navigationExtras: NavigationExtras = {
      state: {
        parametros: this.data2
      }
    };
    this.router.navigate(['formulario3'], navigationExtras);
  
}

}

  


