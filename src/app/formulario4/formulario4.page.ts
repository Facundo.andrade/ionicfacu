import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-formulario4',
  templateUrl: './formulario4.page.html',
  styleUrls: ['./formulario4.page.scss'],
})
export class Formulario4Page{

  data4:any;
  data5:any;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {

        this.data4 = this.router.getCurrentNavigation().extras.state.parametros;
        this.data5 = this.router.getCurrentNavigation().extras.state.parametros2;  

      }
    });
  }

}
