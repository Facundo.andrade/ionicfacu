import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Formulario3PageRoutingModule } from './formulario3-routing.module';

import { Formulario3Page } from './formulario3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Formulario3PageRoutingModule
  ],
  declarations: [Formulario3Page]
})
export class Formulario3PageModule {}
