import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-listaapp',
  templateUrl: './listaapp.page.html',
  styleUrls: ['./listaapp.page.scss'],
})


export class ListaappPage implements OnInit {

  usuarios: Observable<any>;

  constructor(private dataService: DataService) { }

  ngOnInit() {

    this.usuarios = this.dataService.getUsuarios();

  }

}
