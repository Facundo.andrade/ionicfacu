import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaappPageRoutingModule } from './listaapp-routing.module';

import { ListaappPage } from './listaapp.page';

import {HttpClientModule} from '@angular/common/http'
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],

  declarations: [ListaappPage]
})
export class ListaappPageModule {}




  



