import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { info } from 'src/app/formulario/formulario.page';


@Component({
  selector: 'app-formulario3',
  templateUrl: './formulario3.page.html',
  styleUrls: ['./formulario3.page.scss'],
})
export class Formulario3Page {

  data3:any;
  dadescard: credit = new credit();

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data3 = this.router.getCurrentNavigation().extras.state.parametros;
      }
    });
  }
  
  pasoDatos2() {

    console.log(this.data3);
    console.log(this.dadescard);

      let navigationExtras: NavigationExtras = {
        state: {
          parametros: this.data3,
          parametros2: this.dadescard
        }
      };
      this.router.navigate(['formulario4'], navigationExtras);
    
  }

}

export class credit {

  cardnum: string = "";
  cardname: string = "";
  ExpMonth: string = "";
  ExpYear: string = "";
  cardcvv: string = "";

  constructor() { }

}

