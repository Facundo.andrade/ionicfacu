import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Formulario4Page } from './formulario4.page';

const routes: Routes = [
  {
    path: '',
    component: Formulario4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Formulario4PageRoutingModule {}
