import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaappPage } from './listaapp.page';

const routes: Routes = [
  {
    path: '',
    component: ListaappPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaappPageRoutingModule {}
