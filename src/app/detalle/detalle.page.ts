import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})

export class DetallePage implements OnInit {

  data:any;
  data2:any;
  data3:any;
  data4:any;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        this.data2 = this.router.getCurrentNavigation().extras.state.parametros2;

        var input = document.getElementById("palabra") as HTMLInputElement;
        input.value = this.data;

        var input = document.getElementById("coche") as HTMLInputElement;
        input.value = this.data2;

        this.data3 = this.data2.marca;

        this.data4 = this.data2.matricula;
      }
    });
  }


  ngOnInit() {
  }

}
