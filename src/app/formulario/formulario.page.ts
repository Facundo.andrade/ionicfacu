import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.page.html',
  styleUrls: ['./formulario.page.scss'],
})
export class FormularioPage {

  constructor(private router: Router) { }

  datosform: object = new info();

  dades: info = new info();

  emailError: boolean = false


  pasoDatos() {

    if (this.dades.email.length === 0) {

      this.emailError = true;

    } else {
      let navigationExtras: NavigationExtras = {
        state: {
          parametros: this.dades
        }
      };
      this.router.navigate(['formulario2'], navigationExtras);
    }
  }
}

export class info {

  promo: string = "";
  email: string = "";
  nombre: string = "";
  apellidos: string = "";
  empresa: string = "";
  dir1: string = "";
  dir2: string = "";
  pais: string = "";
  ciudad: string = "";
  provincia: string = "";
  cp: string = "";
  tel: string = "";

  constructor() { }

}
