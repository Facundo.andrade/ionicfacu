import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {


  constructor(private router: Router) {}

  pasoParametro(){

    var mitest : string = "Hola este es mi string";
    var coche1 = new coche();
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: mitest,
        parametros2: coche1,
      }
    };
    this.router.navigate(['detalle'], navigationExtras);
  }

}

export class coche{
  marca: string = "Mini";
  model: string = "Copper";
  matricula: string = "BXL 6677";
  ruedas: number = 4;

  constructor( ) {}

  muestraInf() {
    return "Este coche es de la marca: "+this.marca+" y del model: "+this.model;
  }


}
